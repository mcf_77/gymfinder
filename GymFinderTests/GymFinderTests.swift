//
//  GymFinderTests.swift
//  GymFinderTests
//
//  Created by Marcel McFall on 12/1/18.
//  Copyright © 2018 Marcel McFall. All rights reserved.
//

import XCTest
@testable import GymFinder
import Quick
import Nimble
import RxSwift

class GymFinderTests: QuickSpec {
    override func spec() {
        describe("the GymCellModel object") {
            it("contains a valid name and address") {
                let viewModel = GymViewModel()
                let locations:Variable<[Gym?]?> = Variable(nil)
                let gyms:[Gym?] = [Gym("FitnessFirst", address:"123 Main Street")]
                locations.value = gyms
                viewModel.gymLocations = locations
                let gymCellModel = viewModel.cellViewModel(forIndex: 0)
                expect(gymCellModel?.gymName).to(contain("FitnessFirst"))
                expect(gymCellModel?.address).to(contain("123 Main Street"))
                expect(gymCellModel?.address.count) > 0
            }
        }
    }
}
