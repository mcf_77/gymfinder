//
//  GymCellModal.swift
//  GymFinder
//
//  Created by Marcel McFall on 14/1/18.
//  Copyright © 2018 Marcel McFall. All rights reserved.
//

import Foundation
struct GymCellModel {
    let gym:Gym
    
    init(gym: Gym ) {
        self.gym = gym
    }
    var gymName: String {
        return gym.name
    }
    
    var address: String {
        return gym.address
    }
}
