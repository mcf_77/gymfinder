//
//  GymTableViewCell.swift
//  GymFinder
//
//  Created by Marcel McFall on 14/1/18.
//  Copyright © 2018 Marcel McFall. All rights reserved.
//

import UIKit

class GymTableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
