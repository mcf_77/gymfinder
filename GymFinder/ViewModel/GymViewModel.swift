//
//  GymViewModel.swift
//  GymFinder
//
//  Created by Marcel McFall on 12/1/18.
//  Copyright © 2018 Marcel McFall. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift

class GymViewModel {
    var gymLocations:Variable<[Gym?]?> = Variable(nil)
    var gymService: GymService
    
    init(_ gymService: GymService = GymService()) {
        self.gymService = gymService
    }

    func findNearbyGym() {
        gymService.fetchGymsAroundMe({ [weak self] (gyms) in
            guard let gymLocations = gyms else {
                print("missing gyms")
                return
            }
            self?.gymLocations.value = gymLocations
            return
        })
    }
    
    func cellViewModel(forIndex: Int) -> GymCellModel? {
        guard let gyms = self.gymLocations.value, let gym = gyms[forIndex] else { return nil }
        return GymCellModel(gym: gym)
    }
    
    var gymCount: Int {
        guard let gyms = (gymLocations.value?.count) else { return 0}
        return gyms
    }
}

