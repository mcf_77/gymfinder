//
//  GymViewController.swift
//  GymFinder
//
//  Created by Marcel McFall on 12/1/18.
//  Copyright © 2018 Marcel McFall. All rights reserved.
//

import UIKit
import CoreLocation
import RxSwift

class GymViewController: UIViewController {
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var gymTable: UITableView!
    
    let cellIdentifier = "gymCell"
    let disposeBag = DisposeBag()
    var viewModel:GymViewModel = GymViewModel()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRx()
        
        let nib = UINib(nibName: "GymTableViewCell", bundle: nil)
        gymTable.register(nib, forCellReuseIdentifier: "gymCell")
        viewModel.findNearbyGym()
    }
    
    func setupRx() {
        viewModel.gymLocations.asObservable().subscribe(onNext: { [weak self] (gyms) in
            guard let _ = gyms else { return }
            self?.gymTable.reloadData()
            self?.activity.stopAnimating()
            }).disposed(by: disposeBag)
    }

}
extension GymViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? GymTableViewCell
        guard let cellModel = viewModel.cellViewModel(forIndex: indexPath.row) else { return UITableViewCell() }
        cell?.name.text = cellModel.gymName
        cell?.address.text = cellModel.address
        return cell!
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.gymCount
    }
}

