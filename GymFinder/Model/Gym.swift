//
//  Gym.swift
//  GymFinder
//
//  Created by Marcel McFall on 13/1/18.
//  Copyright © 2018 Marcel McFall. All rights reserved.
//

import Foundation
struct Gym {
    var name:String
    var address:String
    
    init(_ name:String, address:String) {
        self.name = name
        self.address = address
    }
}
extension Gym {
    
}
