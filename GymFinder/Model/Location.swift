//
//  Location.swift
//  GymFinder
//
//  Created by Marcel McFall on 14/1/18.
//  Copyright © 2018 Marcel McFall. All rights reserved.
//

import Foundation
struct Location {
    let latitude:String
    let longitude:String
    
    init(_ latitude:String, longitude:String) {
        self.latitude = latitude
        self.longitude = longitude
    }
}
