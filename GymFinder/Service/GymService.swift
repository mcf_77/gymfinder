//
//  GymService.swift
//  GymFinder
//
//  Created by Marcel McFall on 12/1/18.
//  Copyright © 2018 Marcel McFall. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct GymURL {
    static let baseURL = "https://private-anon-bbc0b3b729-fitlgdemo.apiary-mock.com"
    static let nearby = baseURL + "/api/v1/gyms"
}

class GymService: APIService {
    let locationService: LocationService
    
    init(locationService: LocationService = LocationService()) {
        self.locationService = locationService
    }
    
    func fetchGymsAroundMe(_ completionBlock: @escaping([Gym?]?) -> Void) {
        locationService.findUsersLocation(completionBlock: { [weak self] (location) in
            self?.fetchGymsForLocation(location, completionBlock: { (gyms) in
                completionBlock(gyms)
            })
        })
    }
    func fetchGymsForLocation(_ location:Location?, completionBlock: @escaping([Gym?]?) -> Void) {
        guard let location = location else { return completionBlock(nil)}
        let params: [String:String] = ["latitude":location.latitude, "longitude":location.longitude]
        self.fetchData(WithUrl: GymURL.nearby, parameters: params, completionBlock: { [weak self] (result) in
            guard let gyms = result else {
                completionBlock(nil)
                return
            }
            let json = JSON(gyms)
            let gymLocations = self?.convertJsonGym(json)
            completionBlock(gymLocations)
        })
    }
}
extension GymService {
    func convertJsonGym(_ json:JSON) -> [Gym?] {
        let gyms = json.map { (_, json) -> Gym? in
            guard let name = json["name"].string, let address = json["address"].string else {
                print("invalid json")
                return nil
            }
            return Gym(name, address:address)
        }
        return gyms
    }
}
