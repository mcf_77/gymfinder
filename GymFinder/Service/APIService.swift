//
//  APIService.swift
//  GymFinder
//
//  Created by Marcel McFall on 14/1/18.
//  Copyright © 2018 Marcel McFall. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire
enum RequestType:String {
    case Get
    case Post
}

class APIService: NSObject {
    public func fetchData(WithUrl url:String, parameters:[String:String], completionBlock: @escaping(Any?) -> Void) {
        guard let url = URL(string:url) else {
            print("Invalid URL String!")
            return
        }
        Alamofire.request(url, method: .post, parameters: parameters).validate().responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    print("Error while fetching nearest Gyms: \(response.result.error.debugDescription)")
                    completionBlock(nil)
                    return
                }
                
            guard let result = response.result.value else {
                    print("Malformed data received from api")
                    completionBlock(nil)
                    return
                }
            completionBlock(result)
            return
        }
    }
}
