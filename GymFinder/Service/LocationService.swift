//
//  LocationService.swift
//  GymFinder
//
//  Created by Marcel McFall on 12/1/18.
//  Copyright © 2018 Marcel McFall. All rights reserved.
//

import Foundation
import CoreLocation

class LocationService : NSObject {
    let locationManager = CLLocationManager()
    func findUsersLocation(completionBlock: @escaping (Location?) -> Void) {
        guard let location = currentLocation else {
            print("Unable to retrieve Location")
            completionBlock(nil)
            return
        }
        completionBlock(location)
    }
}
extension LocationService: CLLocationManagerDelegate {
    var currentLocation:Location? {
        guard let latitude = locationManager.location?.coordinate.latitude, let longitude = locationManager.location?.coordinate.longitude else { return nil }
        let lat = String(format:"%f", latitude)
        let long = String(format:"%f", longitude)
        return Location(lat, longitude:long)
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus){
        guard (manager.location?.coordinate.latitude) != nil else {
            print("Did not get Location")
            return
        }
    }
}
